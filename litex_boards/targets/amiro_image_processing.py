#!/usr/bin/env python3

from platform import platform
from litex.soc.integration.common import *
from litex.soc.integration.soc import *
from litescope import LiteScopeAnalyzer
from migen import *
from migen.genlib.resetsync import AsyncResetSynchronizer
from litex.soc.integration.soc_core import *
from litex.soc.integration.builder import *
from litedram.modules import MT46H64M16
from litedram.phy import s6ddrphy

import sys
sys.path.insert(0,"../../../../litebusmonitor/litebusmonitor")
sys.path.insert(0,"../..")
from litebusmonitor.monitor import LiteBusMonitor
from litebusmonitor.receiver import LiteBusReceiver


from fractions import Fraction
import os
import argparse
import importlib

class _CRG(Module):
    def __init__(self, platform, sys_clk_freq, pll_clk_input_div):
        self.rst = Signal()
        self.clock_domains.cd_sys           = ClockDomain()
        self.clock_domains.cd_sdram_half    = ClockDomain()
        self.clock_domains.cd_sdram_full_wr = ClockDomain()
        self.clock_domains.cd_sdram_full_rd = ClockDomain()
        self.clock_domains.cd_periph = ClockDomain()

        self.reset = Signal()

        # # #

        # Input clock ------------------------------------------------------------------------------
        PLL_INPUT_freq = int(100e6/pll_clk_input_div)
        clk100      = platform.request("clk100")
        PLL_INPUT     = Signal()
        self.specials += Instance("BUFIO2",
            p_DIVIDE=pll_clk_input_div, p_DIVIDE_BYPASS="TRUE",
            p_I_INVERT="FALSE",
            i_I=clk100, o_DIVCLK=PLL_INPUT)

        # PLL --------------------------------------------------------------------------------------
        pll_lckd           = Signal()
        pll_fb             = Signal()
        pll_sdram_full   = Signal()
        pll_sdram_half_a = Signal()
        pll_sdram_half_b = Signal()
        pll_unused       = Signal()
        pll_sys          = Signal()
        pll_periph       = Signal()


        # dumb automatic p finding:
        best_p = 0
        best_vco = 0
        for p in range(1,30):
            f = Fraction(sys_clk_freq*p, PLL_INPUT_freq)
            n, d = f.numerator, f.denominator
            pfd = PLL_INPUT_freq/d
            vco = PLL_INPUT_freq*n/d
            if 19e6 <= PLL_INPUT_freq/d <= 500e6 and 400e6 <= PLL_INPUT_freq*n/d <= 1000e6:
                if best_vco <= vco:
                    best_vco = vco
                    best_p = p
        p = best_p
        print(f"Best p:{p}, vco: {best_vco}")
        #Working p: 8 ==> {100,80}mhz; 12 ==> ~83mhz
        f = Fraction(sys_clk_freq*p, PLL_INPUT_freq)
        n, d = f.numerator, f.denominator
        print("n:%d, d:%d" % (n,d))
        assert 19e6 <= PLL_INPUT_freq/d <= 500e6, PLL_INPUT_freq/d  # pfd
        assert 400e6 <= PLL_INPUT_freq*n/d <= 1000e6, PLL_INPUT_freq*n/d  # vco

        self.specials.pll = Instance(
            "PLL_ADV",
            name="crg_pll_adv",
            p_SIM_DEVICE="SPARTAN6", p_BANDWIDTH="OPTIMIZED", p_COMPENSATION="INTERNAL",
            p_REF_JITTER=.01,
            i_DADDR=0, i_DCLK=0, i_DEN=0, i_DI=0, i_DWE=0, i_RST=0, i_REL=0,
            p_DIVCLK_DIVIDE=d,
            # Input Clocks (100MHz)
            i_CLKIN1=PLL_INPUT,
            p_CLKIN1_PERIOD=1e9/PLL_INPUT_freq,
            i_CLKIN2=0,
            p_CLKIN2_PERIOD=0.,
            i_CLKINSEL=1,
            # Feedback clk100*n = 800mhz for n=8
            i_CLKFBIN=pll_fb, o_CLKFBOUT=pll_fb, o_LOCKED=pll_lckd,
            p_CLK_FEEDBACK="CLKFBOUT",
            p_CLKFBOUT_MULT=n, p_CLKFBOUT_PHASE=0.,
            # (333MHz) sdram wr rd
            o_CLKOUT0=pll_sdram_full, p_CLKOUT0_DUTY_CYCLE=.5,
            p_CLKOUT0_PHASE=0., p_CLKOUT0_DIVIDE=p//4,
            # unused?
            o_CLKOUT1=pll_unused, p_CLKOUT1_DUTY_CYCLE=.5,
            p_CLKOUT1_PHASE=0., p_CLKOUT1_DIVIDE=15,
            # (166MHz) sdram_half - sdram dqs adr ctrl
            o_CLKOUT2=pll_sdram_half_a, p_CLKOUT2_DUTY_CYCLE=.5,
            p_CLKOUT2_PHASE=270., p_CLKOUT2_DIVIDE=p//2,
            # (166MHz) off-chip ddr
            o_CLKOUT3=pll_sdram_half_b, p_CLKOUT3_DUTY_CYCLE=.5,
            p_CLKOUT3_PHASE=250., p_CLKOUT3_DIVIDE=p//2, 
            # ( 32MHz) periph
            o_CLKOUT4=pll_periph, p_CLKOUT4_DUTY_CYCLE=.5,
            p_CLKOUT4_PHASE=0., p_CLKOUT4_DIVIDE=25,
            # ( 83MHz) sysclk
            o_CLKOUT5=pll_sys, p_CLKOUT5_DUTY_CYCLE=.5,
            p_CLKOUT5_PHASE=0., p_CLKOUT5_DIVIDE=p//1,
        )

        # Power on reset
        reset = self.reset | self.rst #platform.request("user_btn") |
        self.clock_domains.cd_por = ClockDomain()
        por = Signal(max=1 << 11, reset=(1 << 11) - 1)
        self.sync.por += If(por != 0, por.eq(por - 1))
        self.specials += AsyncResetSynchronizer(self.cd_por, reset)

        # System clock
        self.specials += Instance("BUFG", i_I=pll_sys, o_O=self.cd_sys.clk)
        self.comb += self.cd_por.clk.eq(self.cd_sys.clk)
        self.specials += AsyncResetSynchronizer(self.cd_sys, ~pll_lckd | (por > 0))

        # periph clock
        self.specials += Instance("BUFG", i_I=pll_periph, o_O=self.cd_periph.clk)
        #self.comb += self.cd_periph.clk.eq(pll_periph)

        # SDRAM clocks -----------------------------------------------------------------------------
        self.clk4x_wr_strb = Signal()
        self.clk4x_rd_strb = Signal()

        # SDRAM full clock
        self.specials += Instance("BUFPLL", name="sdram_full_bufpll",
            p_DIVIDE       = 4,
            i_PLLIN        = pll_sdram_full, i_GCLK=self.cd_sys.clk,
            i_LOCKED       = pll_lckd,
            o_IOCLK        = self.cd_sdram_full_wr.clk,
            o_SERDESSTROBE = self.clk4x_wr_strb)
        self.comb += [
            self.cd_sdram_full_rd.clk.eq(self.cd_sdram_full_wr.clk),
            self.clk4x_rd_strb.eq(self.clk4x_wr_strb),
        ]
        # SDRAM_half clock
        self.specials += Instance("BUFG", name="sdram_half_a_bufpll",
            i_I=pll_sdram_half_a, o_O=self.cd_sdram_half.clk)
        clk_sdram_half_shifted = Signal()
        self.specials += Instance("BUFG", name="sdram_half_b_bufpll",
            i_I=pll_sdram_half_b, o_O=clk_sdram_half_shifted)
        clk = platform.request("ddram_clock")
        self.specials += Instance("ODDR2", p_DDR_ALIGNMENT="NONE",
            p_INIT=0, p_SRTYPE="SYNC",
            i_D0=1, i_D1=0, i_S=0, i_R=0, i_CE=1,
            i_C0=clk_sdram_half_shifted,
            i_C1=~clk_sdram_half_shifted,
            o_Q=clk.p)
        self.specials += Instance("ODDR2", p_DDR_ALIGNMENT="NONE",
            p_INIT=0, p_SRTYPE="SYNC",
            i_D0=0, i_D1=1, i_S=0, i_R=0, i_CE=1,
            i_C0=clk_sdram_half_shifted,
            i_C1=~clk_sdram_half_shifted,
            o_Q=clk.n)

# BaseSoC ------------------------------------------------------------------------------------------

class BaseSoC(SoCCore):
    def __init__(self, platform, sys_clk_freq, pll_clk_input_div, **kwargs):
        # SoCCore ----------------------------------------------------------------------------------
        SoCCore.__init__(self, platform, sys_clk_freq,
            ident          = "LiteX SoC on AMiRO @ImageProcessing",
            **kwargs)
        self.pll_clk_input_div = pll_clk_input_div
        # CRG --------------------------------------------------------------------------------------
        self.submodules.crg = _CRG(platform, self.sys_clk_freq, self.pll_clk_input_div)
        self.platform.add_period_constraint(self.crg.cd_sys.clk, 1e9/self.sys_clk_freq)

        #self.add_ram("main_ram", origin=0x20000000, size=8192*2)
        #self.submodules.can = can = cantop.CanBus(platform)
        #self.add_csr("can")
        #self.bus.add_slave('can', can.wb_int, SoCRegion(origin=0x20000, size=0x100, mode="rw")) # kA how many bytes we need

        #analyzer_signals = [
        #    can.wb_rst_i,
        #    can.wb_dat_i,
        #    can.wb_dat_o,
        #    can.wb_cyc_i,
        #    can.wb_stb_i,
        #    can.wb_we_i,
        #    can.wb_adr_i,
        #    can.wb_ack_o,
        #    can.irq_on,
        #]
        #self.submodules.analyzer = LiteScopeAnalyzer(analyzer_signals,
        #    depth        = 4096,
        #    clock_domain = "sys",
        #    csr_csv      = "analyzer.csv")
        #self.add_csr("analyzer")

    def add_monitor(self, ms="m"):
        if ms == "m":
            self.submodules.monitor = m = LiteBusMonitor(bus=self.bus, 
                dataPin_out=self.platform.request("monitor_pin_master", 0), 
                dataValid_out=self.platform.request("monitor_pin_master", 1), 
                receiverReady_in=self.platform.request("monitor_pin_master", 2))
        elif ms == "s":
            self.submodules.monitor = m = LiteBusMonitor(bus=self.bus, 
                dataPin_out=self.platform.request("monitor_pin_master", 0), 
                dataValid_out=self.platform.request("monitor_pin_master", 1), 
                receiverReady_in=self.platform.request("monitor_pin_master", 2))

            self.submodules.receiver = LiteBusReceiver(
                dataPin_in=self.platform.request("monitor_pin_slave", 0),
                dataValid_in=self.platform.request("monitor_pin_slave", 1),
                receiverReady_out=self.platform.request("monitor_pin_slave", 2),
                queueLayout=m.getQueueLayoutDict())


        # 0000128e24 unterer
        # 0000186 oberer



    def add_dram(self, l2_size):
        # LPDDR SDRAM ------------------------------------------------------------------------------
        self.submodules.ddrphy = s6ddrphy.S6HalfRateDDRPHY(self.platform.request("ddram"),
            memtype           = "LPDDR",
            rd_bitslip        = 1,
            wr_bitslip        = 3,
            dqs_ddr_alignment = "C1")
        self.comb += [
            self.ddrphy.clk4x_wr_strb.eq(self.crg.clk4x_wr_strb),
            self.ddrphy.clk4x_rd_strb.eq(self.crg.clk4x_rd_strb),
        ]
        self.add_sdram("sdram",
            phy           = self.ddrphy,
            module        = MT46H64M16(self.sys_clk_freq, "1:2"),
            l2_cache_size = l2_size
        )


# Build --------------------------------------------------------------------------------------------

def main(arguments: list[str]):
    parser = argparse.ArgumentParser(description="LiteX SoC on AMiRo Imageprocessing Board")
    parser.add_argument("--build",        action="store_true", help="Build bitstream")
    parser.add_argument("--load",         action="store_true", help="Load bitstream")
    parser.add_argument("--platform", help="Module name of the platform to build for", default='litex_boards.platforms.amiro_image_processing')
    parser.add_argument("--toolchain", default=None, help="FPGA gateware toolchain used for build")
    parser.add_argument("--pll-clk-input-div", default=1)
    parser.add_argument("--sys-clk-freq", default=80*1000*1000, type=int)
    parser.add_argument("--ident-version", default=True)
    parser.add_argument("--with-dram",         action="store_true", help="Enable DRAM")

    parser.add_argument("--with-monitor-master", action="store_true", help="Enable bus monitoring")
    parser.add_argument("--with-monitor-slave", action="store_true", help="Enable bus monitor receiving")

    sdopts = parser.add_mutually_exclusive_group()
    sdopts.add_argument("--with-sdcard",         action="store_true", help="Enable SDCard support")
    
    builder_args(parser)
    soc_core_args(parser)
    args = parser.parse_args(arguments)

    platform_module = importlib.import_module(args.platform)
    if args.toolchain is not None:
        platform = platform_module.Platform(toolchain=args.toolchain)
    else:
        platform = platform_module.Platform()

    soc = BaseSoC(platform, args.sys_clk_freq, args.pll_clk_input_div, **soc_core_argdict(args))
    if args.with_dram:
        soc.add_dram(args.l2_size) #, 8192*2)
    if args.with_sdcard:
        soc.add_sdcard()
    
    if args.with_monitor_master:
        soc.add_monitor("m")
    if args.with_monitor_slave:
        soc.add_monitor("s")

    builder = Builder(soc, **builder_argdict(args))
    builder.build(run=args.build)

    if args.load:
        prog = soc.platform.create_programmer()
        prog.load_bitstream(os.path.join(builder.gateware_dir, soc.build_name + ".bit"))

if __name__ == "__main__":
    main(sys.argv[1:])
